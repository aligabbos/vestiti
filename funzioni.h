#ifndef FUNZIONI
#define FUNZIONI

typedef struct{
  unsigned int type;
  unsigned int size;
  unsigned int quantity;
  float price;
} vestito;

typedef struct{
  vestito* ves;
  unsigned int dim;
} vestiti;

unsigned int leggi(char*, vestiti*);
void estrai(unsigned int, vestiti, vestiti*);
unsigned int quant(vestiti);
float prezzo(vestiti);
unsigned int scrivi(char*, vestiti);

#endif //FUNZIONI