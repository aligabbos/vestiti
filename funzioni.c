#include<stdio.h>
#include<stdlib.h>
#include <errno.h>
#include "funzioni.h"

unsigned int leggi(char* file_name, vestiti* v_file){
  FILE* fp = fopen(file_name, "r");
  int char_file, i;
  char str[60];

  //controllo se il file esiste
  if(fp){
    v_file->dim = 0;

    //conto le righe del file
    while(fgets(str, 60, fp) != NULL){
      v_file->dim += 1;
    }

    if(v_file->dim > 20)
      v_file->dim = 20;

    //riporto l'indice del file all'inizio
    fseek(fp, 0, SEEK_SET);

    //alloco spazio
    v_file->ves = (vestito*) malloc(sizeof(vestito) * v_file->dim);

    for(i = 0; i < v_file->dim; i++){
      fscanf(fp, "%d %d %d %f", &v_file->ves[i].type, &v_file->ves[i].size, &v_file->ves[i].quantity, &v_file->ves[i].price);
    }

    fclose(fp);

    return 1;

    /*char_file = fgetc(fp);
    while(char_file != EOF){
      char_file = fgetc(fp);

      if(!feof(fp)){
        fseek(fp, ftell(fp) - 2, SEEK_SET);
        fscanf(fp, "%d %d %d %.2f", &v_file., &b);
        printf("%d %d\n", a, b);
        char_file = fgetc(fp);
      }
    }*/
  } else
    perror("Error");

  return 0;
}

void estrai(unsigned int size, vestiti v_file, vestiti* v_size){
  int i, j = 0;
  v_size->dim = 0;

  //conto la quantità degli elementi con taglia = size
  for(i = 0; i < v_file.dim; i++)
    if(size == v_file.ves[i].size)
      v_size->dim += 1;

  v_size->ves = (vestito*) malloc(sizeof(vestito) * v_size->dim);

  //copio gli elementi con taglia = size
  for(i = 0; i < v_file.dim; i++)
    if(size == v_file.ves[i].size){
      v_size->ves[j] = v_file.ves[i];
      j++;
    }

}

unsigned int quant(vestiti v_qnty){
  unsigned int tot = 0;
  int i;

  for(i = 0; i < v_qnty.dim; i++)
    tot += v_qnty.ves[i].quantity;

  return tot;
}

float prezzo(vestiti v_prz){
  float tot = 0;
  int i;

  for(i = 0; i < v_prz.dim; i++)
    tot += (v_prz.ves[i].quantity * v_prz.ves[i].price);

  return tot;
}

unsigned int scrivi(char* file_name, vestiti v){
  FILE* fp = fopen(file_name, "w");
  int i;
  vestiti v_search;

  if(fp){
    for(i = 42; i <= 54; i += 2){
      estrai(i, v, &v_search);
      
      if(v_search.dim)
        fprintf(fp, "%d %d %f\n", i, quant(v_search), prezzo(v_search));
    }
    
    fclose(fp);

    return 1; 
  }else
    perror("Error");

  return 0;
}