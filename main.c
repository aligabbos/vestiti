#include <stdio.h>
#include <stdlib.h>
#include "funzioni.h"

int main(void) {
  vestiti vs;

  if ( leggi("vestiti.txt", &vs) ){
    if ( ! ( scrivi("totali.txt", vs) ) )
      exit(EXIT_FAILURE);
  }else
    exit(EXIT_FAILURE);

  exit(EXIT_SUCCESS);
}